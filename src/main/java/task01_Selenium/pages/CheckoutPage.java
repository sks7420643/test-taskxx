package task01_Selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import task01_Selenium.models.User;

public class CheckoutPage extends BasePage {

    @FindBy(id = "first-name")
    private WebElement firstnameInput;

    @FindBy(id = "last-name")
    private WebElement lastnameInput;

    @FindBy(id = "postal-code")
    private WebElement zipInput;

    @FindBy(id = "continue")
    private WebElement continueButton;

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public CheckoutPage enterFirstname(String firstname) {
        firstnameInput.sendKeys(firstname);
        return this;
    }

    public CheckoutPage enterLastname(String lastname) {
        lastnameInput.sendKeys(lastname);
        return this;
    }

    public CheckoutPage enterZipCode(String zipCode) {
        zipInput.sendKeys(zipCode);
        return this;
    }

    public CheckoutPage clickOnContinueButton(){
        continueButton.click();
        return this;
    }

    public CheckoutOverviewPage enterCheckoutData(User user){
        enterFirstname(user.getFirstname())
                .enterLastname(user.getLastname())
                .enterZipCode(user.getZip())
                .clickOnContinueButton();
        return new CheckoutOverviewPage(driver);
    }
}
