package task01_Selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.jupiter.api.Assertions.*;

public class CheckoutOverviewPage extends BasePage{

    @FindBy(id = "finish")
    private WebElement finishButton;

    @FindBy(className = "summary_total_label")
    private WebElement totalPrice;


    public CheckoutOverviewPage(WebDriver driver) {
        super(driver);
    }

    public CheckoutCompletePage clickOnFinishButton() {
        finishButton.click();
        return new CheckoutCompletePage(driver);
    }

    public String getTotalPrice(){
        return totalPrice.getText();
    }

    public CheckoutOverviewPage verifyTotalPrice(String totalPrice){
        assertTrue(getTotalPrice().contains(totalPrice));
        return this;
    }
}
