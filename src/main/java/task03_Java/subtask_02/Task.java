package task03_Java.subtask_02;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Task {

    //Write a Java method to find all of the longest string in a given array of strings.
    //e.g.
    //input: {"cat", "dog", "red", "is", "am"}
    //result: [cat, dog, red]

    public static void main(String[] args) {
        String[] input = {"cat", "dog", "red", "is", "am", "qwert", "five", "qwasd"};
        List<String> result = findLongestStrings(input);
        System.out.println("Result: " + result);
    }

    public static List<String> findLongestStrings(String[] strings){
        int maxLength = Arrays.stream(strings)
                .mapToInt(String::length)
                .max()
                .orElse(0);

        return Arrays.stream(strings)
                .filter(str -> str.length() == maxLength)
                .collect(Collectors.toList());
    }
}
