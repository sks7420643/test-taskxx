package task03_Java.subtask_03;

import java.util.ArrayList;
import java.util.Arrays;

public class Box<T> {

    //Create a generic class called Box<T> that can store an object of any type T. Include methods to
    //set and get the value of the object.
    private T value;

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public static void main(String[] args) {

        Box<Double> doubleBox = new Box<>();
        doubleBox.setValue(19292.234);

        Box<ArrayList<Integer>> listBox = new Box<>();
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        listBox.setValue(arrayList);

        Box<String []> arrayBox = new Box<>();
        String[] array = {"Cat", "Dog"};
        arrayBox.setValue(array);

        System.out.println("Double box value: " + doubleBox.getValue());
        System.out.println("List box value: " + listBox.getValue());
        System.out.println("String array value: " + Arrays.toString(arrayBox.getValue()));
    }
}
